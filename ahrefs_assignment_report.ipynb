{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Ahrefs Search ranking Keyword analysis\n",
    "- Technical assignement for the role of Data Scientist \n",
    "- Study was done in Python using pandas, nltk, seaborn and scipy among others\n",
    "- Sept 2017\n",
    "- (c) Adarsh Janakiraman"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task\n",
    "We are going to conduct a study on how anchor text and surrounding text from backlinks affects how google ranks search results for different keywords. More specifically, we are looking at how the search rankings for **backlinko.com** are impacted by the relationship between the text (anchor and surrounding) from the backlinks and the keywords which Google ranks the website in the top 10 results. The ultimate idea is that such an analysis would help us understand the Google search algorithm better.\n",
    "\n",
    "Ahrefs collects data on the search results from all possible combination of keywords and it also collects information on the backlinks which link into the site of interest, in this case backlinko.com. Regarding the backlinks, Ahrefs collects the anchor and surrounding text of the link itself and stores it in its database.\n",
    "\n",
    "This study is going to be broken down into three aspects:\n",
    "1. How anchor text affects Google search ranking for each keyword\n",
    "    - How the search ranking is impacted when Keyword text (partial or exact) is found in the anchor text\n",
    "2. How surrounding text affects Google search ranking for each Keyword\n",
    "    - How the search ranking is impacted when Keyword text (partial or exact) is found in the surrounding text\n",
    "3. What are the kind of Keywords which help a page rank in the top 10 but are not found in anchor or surrounding text\n",
    "    - Are there identifiable features about these Keywords?\n",
    "    \n",
    "**Note**: we **did not** study the presence and / or absence of search keywords from the text in the backlinks itself in this exercise."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Anchor text study"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Anchor text is the text which which carries the hyperlink from other websites to the **website of our interest** (i.e. websites under the domain of backlinko.com). The sites from which these links originate are known as backlinks. Though the implementation details of the Google search algorithm are kept a closely guarded secret, and are subject to constant evolution, the basic premise of the PageRank algorithm, which got Google popular in the first place, relies on the relevance and popularity of not just the source page but also the backlinks which link into it. This is why a search keyword study should include the impacts of the text in the backlinks. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data extraction\n",
    "To limit the extent of this study and remove noise, we only considered Keywords which help rank backlinko.com in the top 10 Google search results. This information was manually extracted from Ahrefs Top Pages feature. After this, we extracted information about the backlinks coming into each of the 43 sites using the Ahrefs API. This information included anchor and surrounding text information. \n",
    "\n",
    "To compare the text between the keywords and the anchor text, we did it in two ways:\n",
    "1. Exact match: The keyword text matches word for word with the anchor text of the backlink\n",
    "2. Phrase match: One or more words from the keyword matches one or more words from the anchor text in the backlink\n",
    "\n",
    "Before calculating either of these metrics, we first needs to clean the text a little to help the comparison be meaningfull. To clean the text on both sides we did the following steps:\n",
    "- Convert all text to lower case\n",
    "- Remove all punctuations\n",
    "- Remove all stopwords (in english, it will be words like : 'the','and','or', etc) which do not give us any information about the content itself, but are purely used for gramitical purposes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Keywords found in anchor text\n",
    "The exact_match and phrase_match metrics basically give us information regarding how related the search keywords are to the anchor text. If we look at the plot of Google search rank Position vs. the Average % of keywords which have exact or phrase match with the anchor text, we can clearly see a trend there. Google ranks the page worse if there is a smaller percentage of Keyword match. This is the case with both exact match and phrase match metrics though to a different extent.\n",
    "\n",
    "Anchor text exact match with Keyword | Anchor text phrase match with Keyword\n",
    ":-------------------------:|:-------------------------: |\n",
    "![Anchor text Exact match with Keyword](figs/anchor_exact_match_full.png)  | ![Anchor text Phrase match with Keyword](figs/anchor_phrase_match_full.png) \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Key takeaways\n",
    "- As the percentage of keywords with exact or phrase match goes down, the Google search rank becomes worse\n",
    "- This effect is more pronounced in the case of exact matches given the strength of the $R^2$ (coefficient of determination for the regression line). The rule of thumb to evaluate regression line fit is higher  the $R^2$, the better the trend is explained by the straight line in question.\n",
    "- 30-40% of the backlinks have atleast a single phrase match with the keyword which helps the source page rank in the top 10 search results\n",
    "\n",
    "#### Analysis\n",
    "\n",
    "There could be two ways to explain the above observation:\n",
    "1. Google places a high importance on the relevance of the anchor text from backlinks for its ranking algorithm\n",
    "2. There is a third confounding variable which relates both to the anchor text and the search rank position which we have not explored yet.\n",
    "\n",
    "Given the strength of the observation and our prior hunch about the Google PageRank algorithm, it can be safely said that option 1 is a valid theory to explain these observations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Surrounding text study\n",
    "Surrounding text is the text before and after the anchor text on the backlinks (i.e rest of the text on the same sentence). This inforamation is also captured by Ahrefs and is accessible via the same API call used for anchor text. The same data cleaning techniques are used for the surrounding text as those used for the anchor text study. We will analyse the relationship between surrounding text and keywords in the same fashion we analysed the relationship between anchor text and keywords, i.e. with exact_match and phrase_match metrics."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Keywords found in surrounding text\n",
    "We did the same analysis on the relationship between Google search rank and % of backlinks with surrounding text which have the keywords found in them. \n",
    "\n",
    "Surrounding text exact match with Keyword | Surrounding text phrase match with Keyword\n",
    ":-------------------------:|:-------------------------: |\n",
    "![Anchor text Exact match with Keyword](figs/surrounding_exact_match_full.png) | ![Anchor text Phrase match with Keyword](figs/surrounding_phrase_match_full.png) \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Key takeaways\n",
    "- We see a similar relationship between surrounding text with keyword match as that seen in the case of anchor text\n",
    "- The strength of this relationship is much weaker than what was observed in the case of anchor text study. \n",
    "    - The slope of both the regression lines is smaller than the slope of the anchor text study\n",
    "    - The $R^2$ for both lines is quite small (0.35 and 0.18)\n",
    "- As compared to the phrase_match (35-40%) found in the anchor text case, only 20-25% of the backlinks have a phrase_match between surrounding text and search keywords.\n",
    "\n",
    "### Analysis\n",
    "- The slope of the regression line is smaller than the slope of the line in the anchor text study suggesting that the strength (weightage) of the presence or absence of keywords in surrounding text is lower than that attributed to anchor text. \n",
    "- The small $R^2$ value suggests that there is a lot of unexplained variance in this study which may not be explained by simply looking at the % of keywords with phrase or exact match. This evidence re-inforces the idea that surrounding text matches are not that important.\n",
    "- Surrounding text can be much longer than anchor text and typically would be free flowing sentences rather than keyword specific like anchor texts. Thus a realistic study of the correlation/ relationship between keywords and surrounding text should include some form of sentiment analysis where we are able to compare meaning/ context rather than simply the word tokens themselves (as we did in this case)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Missing Keywords study\n",
    "The previous two studies conclusively showed that the Google search rank for a given URL depends on the keywords's similarity to the anchor and surrounding text in the backlinks. But we know that this these two data points are not the only things Google considers for search ranking, evidenced by the point that there are keywords which do not have a single backlink with anchor or surrounding text phrase matches. The table below shows the Keywords which had no matches in anchor and surrounding text. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style>\n",
       "    .dataframe thead tr:only-child th {\n",
       "        text-align: right;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: left;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>anchor_missing</th>\n",
       "      <th>surrounding_missing</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>average monthly searches</th>\n",
       "      <td>Y</td>\n",
       "      <td>N</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>backlinco</th>\n",
       "      <td>N</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>backlinker</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>googlekeyword</th>\n",
       "      <td>Y</td>\n",
       "      <td>N</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>googlekeywordplanner</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>guestblogging</th>\n",
       "      <td>Y</td>\n",
       "      <td>N</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>http yotube</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>it pages</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>key word planer</th>\n",
       "      <td>Y</td>\n",
       "      <td>N</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>keyplanner</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>keywordplanner</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>kyword</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>moretraffic</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>myguestblog</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>quickurl</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>rebranding press release template</th>\n",
       "      <td>Y</td>\n",
       "      <td>N</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>seopage</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>seotools</th>\n",
       "      <td>N</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>skyscaper com</th>\n",
       "      <td>N</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>squeezepage</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>trafficboost</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>viceks</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>videoseo</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>whitehatseo</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>yotub ecom</th>\n",
       "      <td>Y</td>\n",
       "      <td>Y</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                                  anchor_missing surrounding_missing\n",
       "average monthly searches                       Y                   N\n",
       "backlinco                                      N                   Y\n",
       "backlinker                                     Y                   Y\n",
       "googlekeyword                                  Y                   N\n",
       "googlekeywordplanner                           Y                   Y\n",
       "guestblogging                                  Y                   N\n",
       "http yotube                                    Y                   Y\n",
       "it pages                                       Y                   Y\n",
       "key word planer                                Y                   N\n",
       "keyplanner                                     Y                   Y\n",
       "keywordplanner                                 Y                   Y\n",
       "kyword                                         Y                   Y\n",
       "moretraffic                                    Y                   Y\n",
       "myguestblog                                    Y                   Y\n",
       "quickurl                                       Y                   Y\n",
       "rebranding press release template              Y                   N\n",
       "seopage                                        Y                   Y\n",
       "seotools                                       N                   Y\n",
       "skyscaper com                                  N                   Y\n",
       "squeezepage                                    Y                   Y\n",
       "trafficboost                                   Y                   Y\n",
       "viceks                                         Y                   Y\n",
       "videoseo                                       Y                   Y\n",
       "whitehatseo                                    Y                   Y\n",
       "yotub ecom                                     Y                   Y"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import pandas as pd; df=pd.read_pickle('missing_keywords.pkl'); df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Takeaways\n",
    "- There are 22 keywords for which we do not find an anchor match and 20 keywords for which we do not find a surrounding text match, with 17 keywords for which we do not find a phrase match in either.\n",
    "- Most of the Keywords in this list are compounds of two words without the space in between, e.g. 'trafficboost','keywordplanner','moretraffic', etc\n",
    "- There are some keywords which are close typos of proper english words which would have resulted in a keyword match with anchor or surrounding text like 'skyscaper com', 'http yotube' and 'yotub ecom'\n",
    "- Out of 1399 unique keywords which rank backlinko.com in top 10, only 17 of them had no phrase matches in a single backlink. \n",
    "\n",
    "### Analysis\n",
    "- Google has a method to correct for spelling mistakes in the search keyword so search terms which include words like 'yotube' still get backlinko pages ranked in top 10 similar to if the search term included the proper spelling for 'youtube'\n",
    "- To explain how compound keywords like 'trafficboost' get ranked in top 10 pages similar to keywords like 'traffic' and 'boost' seperately, it is fair to assume that Google has a method to break the word down into its meaningful components. It is possible that apart from directly comparing the Keywords as is with the backlinks, Google also does some form of 'fuzzy' matching, whereby its able to match subwords and lemmas with the backlinks\n",
    "- Given that only 17 of the 1399 keywords had no backlinks which had no matches, we can say that Google **weights the availability of exact phrase matches with the backlink text very highly**. Only those compound and or typo keywords which are very close in nature to backlinko's content (seo, traffic, keywords, etc.) make it onto this list. The presence of these keywords in the search must "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "Few main points to summarize:\n",
    "- Both anchor text and surrounding text of a backlink matter to the Google search rank for a given keyword\n",
    "- The presence of the keyword in the anchor text is more important than it's presence in the surrounding text to boost Google search ranking\n",
    "- Keywords where the text has exact match with the anchor or surrounding text is more important than keywords which have only partial match\n",
    "- Google has methods to breakdown compound keywords and extract their meaning. This also applies to typos. It is still able to rank a page highly for a keyword despite not having exact matches in either anchor or surrounding texts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda root]",
   "language": "python",
   "name": "conda-root-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
