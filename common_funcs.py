import pandas as pd
import numpy as np
import nltk
import string
import re
import operator
import sys
#sys.setdefaultencoding('utf8')

def remove_stopwords(sentence):
    from nltk.corpus import stopwords
    words  = sentence.split()
    output = []
    for word in words:
        if word not in stopwords.words('english'):
            output.append(word)
    return ' '.join(output)

def remove_punctuations(sentence):    
    import re   
    regex = re.compile('[%s]' % re.escape(string.punctuation))
    words = sentence.split()
    output = []
    for word in words: 
        word = regex.sub(u'', word)
        if not word == u'':
            output.append(word)
    return ' '.join(output)

def get_tokens_from_terms(sentences):
    import nltk
    text = []
    for sent in sentences:
        sent = remove_punctuations(string.lower(sent))
        sent = remove_stopwords(sent)
        text.append(sent)
    tokens = nltk.word_tokenize(' '.join(text))
    #tokens = remove_punctuations(tokens)
    #tokens = remove_stop_words(tokens)
    return tokens

def get_top_n_tokens(token_dict, n=500):
    import nltk
    return sorted(nltk.FreqDist(token_dict).items(), key=operator.itemgetter(1), reverse=True)[:n]

def phrase_match(keyword, anchor, ngram = 1):
    import nltk
    from nltk.util import ngrams
    keyword_tokens = nltk.word_tokenize(keyword)
    anchor_tokens = nltk.word_tokenize(anchor)
    if ngram > 1:
        keyword_tokens = list(ngrams(keyword_tokens, ngram))
        anchor_tokens = list(ngrams(anchor_tokens, ngram))
    token_intersection = set(keyword_tokens).intersection(set(anchor_tokens))
    return 1 if len(token_intersection) > 0 else 0